import React, { Component } from "react";
import { View, Button, Text, FlatList } from "react-native";
import BarsModule from "./BarsModule";

class BarsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        bars: []
    };    
  }
  componentDidMount() {
    BarsModule.fetchBars((json) => {
        let barsList = JSON.parse(json);
        this.setState({bars: barsList});
    })
  }

  generateBar() {
    let newBar = {
        name: "Nouveau Bar",
        address: "Bruxelles"
    };
    this.setState({bars: [...this.state.bars, newBar]});
  }

  render() {
    return (
        <View>
            <FlatList
                data={ this.state.bars }
                renderItem={({item}) => <Text>{item.name}</Text>} />
            <Button
                title="Add Bar"
                onPress={this.generateBar.bind(this)} />
        </View>
    );
  }
}

export default BarsList;