package com.epirn

import android.util.Log
import com.facebook.react.bridge.Callback
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReactMethod
import com.google.gson.Gson


class Bar(val name: String, val address: String)

class BarsModule(reactContext: ReactApplicationContext) :
    ReactContextBaseJavaModule(reactContext) {

    override fun getName(): String {
        return "BarsModule"
    }

    @ReactMethod
    fun fetchBars(callback: Callback) {
        val gson = Gson()
        val leCubar = Bar("Le Cubar", "Mons")
        val leModerne = Bar("Le Moderne", "Mons")

        val serializedBars = gson.toJson(listOf(leCubar, leModerne))
        callback.invoke(serializedBars)
    }
}
