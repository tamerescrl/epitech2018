/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import BarsList from './BarsList';

export default class App extends Component {
  render() {
    return (
      <BarsList />
    );
  }
}
