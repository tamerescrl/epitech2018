package eu.tamere.apps.epitech

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = arrayOf(Bar::class), version = 1)
abstract class BarDB: RoomDatabase() {

    companion object {
        lateinit var singleton: BarDB

        fun initWith(context: Context) {
            singleton = Room
                .databaseBuilder(context, BarDB::class.java, "epitechDB")
                .allowMainThreadQueries()
                .build()
        }
    }

    abstract fun getBarDao(): BarDao
}