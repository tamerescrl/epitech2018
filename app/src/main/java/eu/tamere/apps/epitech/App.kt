package eu.tamere.apps.epitech

import android.app.Application


class App: Application() {

    override fun onCreate() {
        super.onCreate()
        BarDB.initWith(this)
    }
}