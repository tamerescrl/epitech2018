package eu.tamere.apps.epitech

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface BarDao {

    @Insert(onConflict = REPLACE)
    fun insertBar(bar: Bar)

    @Query("SELECT * FROM bars")
    fun fetchAllBars(): LiveData<List<Bar>>

    @Query("SELECT * FROM bars WHERE id = :idToFetch LIMIT 1")
    fun fetchBar(idToFetch: Int): LiveData<Bar>
}