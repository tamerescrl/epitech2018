package eu.tamere.apps.epitech

import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import eu.tamere.apps.epitech.databinding.ActivityBarDetailsBinding

class BarDetailsViewModel: ViewModel() {
    init {
        Log.d("Bar", "ViewModel created")
    }

    lateinit var bar: Bar
}

class BarDetailsActivity : AppCompatActivity() {

    lateinit var viewModel: BarDetailsViewModel
    lateinit var binding: ActivityBarDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bar_details)

        val bar = if(intent.hasExtra("BAR_KEY")) {
            intent.getSerializableExtra("BAR_KEY") as Bar
        } else {
            Bar(0,"Unknown", "Unknown")
        }

        viewModel = ViewModelProviders
            .of(this)
            .get(BarDetailsViewModel::class.java)

        viewModel.bar = bar
        binding.viewModel = viewModel
    }
}