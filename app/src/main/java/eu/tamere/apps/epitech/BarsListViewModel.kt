package eu.tamere.apps.epitech

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class BarsListViewModel: ViewModel() {

    val barDao: BarDao
    var barsLiveData: LiveData<List<Bar>>

    init {
        barDao = BarDB.singleton.getBarDao()
        barsLiveData = barDao.fetchAllBars()
    }

    fun generateBar() {
        val newBar = Bar(0,"Nouveau Bar", "Bruxelles")
        barDao.insertBar(newBar)
    }
}
