package eu.tamere.apps.epitech

import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class BarAdapter(val context: Context, var bars: List<Bar>): androidx.recyclerview.widget.RecyclerView.Adapter<BarViewHolder>() {

    fun updateBars(bars: List<Bar>) {
        this.bars = bars
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return bars.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BarViewHolder {
        val barView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.list_item_bar, parent, false)

        val handler: (Int) -> Unit = { position: Int ->
            val barToDisplay = bars[position]
            Log.d("BarAdapter", "Bar tapped ${barToDisplay.name}")

            val intent = Intent(context, BarDetailsActivity::class.java)
            intent.putExtra("BAR_KEY", barToDisplay)
            context.startActivity(intent)
        }

        return BarViewHolder(barView, handler)
    }

    override fun onBindViewHolder(holder: BarViewHolder, position: Int) {
        val bar = bars[position]
        holder.barName?.text = bar.name
        Log.d("BarAdapter", "onBindViewHolder: $position")
    }
}

class BarViewHolder(itemView: View, val tapHandler: ((Int) -> Unit)) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    val barName: TextView?

    init {
        barName = itemView?.findViewById(R.id.barName)
        itemView?.setOnClickListener {
            tapHandler.invoke(adapterPosition)
        }
    }
}