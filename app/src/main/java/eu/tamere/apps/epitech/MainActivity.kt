package eu.tamere.apps.epitech

import android.content.Context
import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.*
import eu.tamere.apps.epitech.databinding.ActivityMainBinding
import java.io.Serializable


class MainActivity : AppCompatActivity() {

    external fun intFromJNI(): Int
    external fun stringFromJNI(): String
    external fun barNameLength(bar: Bar): Int
    external fun createBar(name: String, address: String): Bar

    lateinit var adapter: BarAdapter
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d("Bar", "Is beta? ${BuildConfig.IS_BETA}")

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        adapter = BarAdapter(this, listOf<Bar>())

        val viewModel = ViewModelProviders
            .of(this)
            .get(BarsListViewModel::class.java)

        binding.viewModel = viewModel
        binding.barsList.setAdapter(adapter)
        binding.barsList.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))

        viewModel.barsLiveData.observe(this, Observer { bars ->
            adapter.updateBars(bars)
        })

        Log.d("MainActivity", "int from JNI: ${intFromJNI()}")
        Log.d("MainActivity", "string from JNI: ${stringFromJNI()}")

        val bar = Bar(42, "Le Moderne", "Mons")
        Log.d("MainActivity", "Le Moderne.length(): ${barNameLength(bar)}")

        val jniBar = createBar("La Fabrique", "Tournai")
        Log.d("MainActivity", "jniBar: ${jniBar.name}")
    }


    companion object {
        init {
            System.loadLibrary("native-lib")
        }
    }
}
