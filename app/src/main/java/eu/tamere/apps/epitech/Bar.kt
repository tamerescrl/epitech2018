package eu.tamere.apps.epitech

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "bars")
class Bar(@PrimaryKey(autoGenerate = true) var id: Int,
          var name: String,
          var address: String? = null): Serializable {}
