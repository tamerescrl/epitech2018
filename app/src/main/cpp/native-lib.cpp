#include <jni.h>
#include <string>
#include <android/log.h>


extern "C"
JNIEXPORT jint JNICALL Java_eu_tamere_apps_epitech_MainActivity_intFromJNI(
        JNIEnv *env,
        jobject) {
    return 42;
}

extern "C"
JNIEXPORT jstring JNICALL Java_eu_tamere_apps_epitech_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jint JNICALL Java_eu_tamere_apps_epitech_MainActivity_barNameLength(
        JNIEnv *env,
        jobject,
        jobject bar) {

    // Get name (string) from bar
    jclass barClass = env->GetObjectClass(bar);
    jfieldID nameField = env->GetFieldID(barClass, "name", "Ljava/lang/String;");
    jobject barName =  env->GetObjectField(bar, nameField);

    // Call the length() on the string
    jclass stringClass = env->FindClass("java/lang/String");
    jmethodID lengthMethod = env->GetMethodID(stringClass, "length", "()I");
    jint nameLength = env->CallIntMethod(barName, lengthMethod);

    return nameLength;
}


extern "C"
JNIEXPORT jobject JNICALL Java_eu_tamere_apps_epitech_MainActivity_createBar(
        JNIEnv *env,
        jobject,
        jstring name,
        jstring address) {

    jclass barClass = env->FindClass("eu/tamere/apps/epitech/Bar");
    jmethodID methodId = env->GetMethodID(
            barClass,
            "<init>",
            "(ILjava/lang/String;Ljava/lang/String;)V");

    return env->NewObject(barClass, methodId, 0, name, address);
}